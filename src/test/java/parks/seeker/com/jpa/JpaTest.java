package parks.seeker.com.jpa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import parks.seeker.com.domain.Park;
import parks.seeker.com.model.constant.GeneralConstants;
import parks.seeker.com.repository.ParkRepository;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ActiveProfiles(GeneralConstants.DEV_PROFILE)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class JpaTest {

	@Autowired
	ParkRepository parkRepository;

	@Test
	public void should_save_and_read() {
		UUID sharedId = UUID.fromString("c52f23e1-a2c3-485f-9ca9-ffd1cdce529d");
		String code = "testCode";
		Park park = new Park();
		park.setSharedId(sharedId);
		park.setCreatedAt(LocalDateTime.now());
		park.setUpdatedAt(LocalDateTime.now());
		park.setParkCode(code);

		assertFalse(parkRepository.findBySharedId(sharedId).isPresent());

		parkRepository.save(park);
		Park found = parkRepository.findBySharedId(sharedId).orElse(null);
		assertNotNull(found);
		assertEquals(code, found.getParkCode());

		parkRepository.delete(found);

		assertFalse(parkRepository.findBySharedId(sharedId).isPresent());
	}
}
