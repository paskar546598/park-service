package parks.seeker.com.config;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * For testing purposes we use embedded unofficial PostgreSQL embedded database.
 * This configuration will be pushed to Spring Context upon test startup.*
 */
@TestConfiguration
public class TestPersistenceConfig {

    @Bean
    @Primary
    @SneakyThrows
    public DataSource dataSource() {
        EmbeddedPostgres embeddedPostgres = EmbeddedPostgres.builder()
                .setPort(5434)
                .setCleanDataDirectory(true)
                .start();

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDataSource(embeddedPostgres.getPostgresDatabase());
        return dataSource;
    }
}
