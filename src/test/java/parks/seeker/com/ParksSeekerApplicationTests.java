package parks.seeker.com;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import parks.seeker.com.model.constant.GeneralConstants;

/**
 * default microservice test which checks if context has been loaded
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(GeneralConstants.DEV_PROFILE)
public class ParksSeekerApplicationTests {

	@Autowired
	ParksSeekerApplication parksSeekerApplication;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(parksSeekerApplication);
	}

}
