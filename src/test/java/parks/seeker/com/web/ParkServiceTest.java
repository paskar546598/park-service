package parks.seeker.com.web;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.reactive.function.client.WebClient;
import parks.seeker.com.ParksSeekerApplicationTests;
import parks.seeker.com.domain.Park;
import parks.seeker.com.model.dto.mapper.ParkMapper;
import parks.seeker.com.service.crud.ParkCrudService;
import parks.seeker.com.service.web.ParkService;

import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@MockBean(classes = ParkMapper.class)
public class ParkServiceTest extends ParksSeekerApplicationTests {

	@Autowired
	ParkService parkService;

	@MockBean
	ParkCrudService crudService;

	@Mock
	WebClient webClientMock;

	/**
	 * @see ParkService#getParkOnTheFlyByParkCode(String) should not be called if in our DB we have park with this code
	 */
	@Test
	void getByParkCode() {
		String code = "testCode";
		Park park = new Park();
		park.setParkCode(code);

		when(crudService.findParkCode(code)).thenReturn(Optional.of(park));

		parkService.getByParkCode(code);

		verify(webClientMock, times(0)).get();
	}

}
