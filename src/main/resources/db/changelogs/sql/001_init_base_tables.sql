create table if not exists address
(
	id          bigserial    not null
	constraint address_pkey
	primary key,
	created_at  timestamp    not null,
	updated_at  timestamp    not null,
	city        varchar(255),
	line1       varchar(255),
	line2       varchar(255),
	line3       varchar(255),
	postal_code varchar(255),
	state_code  varchar(255),
	type        varchar(255) not null
	);



create table if not exists activity
(
	id         bigserial not null
	constraint activity_pkey
	primary key,
	created_at timestamp not null,
	updated_at timestamp not null,
	name       varchar(255),
	shared_id  uuid      not null
	constraint uk_6vftsf1vybx88dv05f6hhe1tf unique,
	park_id    bigint
	);

create table if not exists email_address
(
	id            bigserial not null
		constraint email_address_pkey
			primary key,
	created_at    timestamp not null,
	updated_at    timestamp not null,
	description   varchar(255),
	email_address varchar(255)
		constraint uk_5013kdle4aaacr9n7j1o829tq unique
);

create table if not exists contact
(
	id               bigserial not null
	constraint contact_pkey
	primary key,
	created_at       timestamp not null,
	updated_at       timestamp not null,
	email_address_id bigint
	constraint fkpy4t39hqq1t5rosr9tejrrr24
	references email_address,
	park_id          bigint,
	phone_number_id  bigint
);

create table if not exists entrance_fee
(
	id          bigserial not null
	constraint entrance_fee_pkey
	primary key,
	created_at  timestamp not null,
	updated_at  timestamp not null,
	cost        varchar(255),
	description varchar(255),
	title       varchar(255)
	);

create table if not exists entrance_pass
(
	id          bigserial not null
	constraint entrance_pass_pkey
	primary key,
	created_at  timestamp not null,
	updated_at  timestamp not null,
	cost        varchar(255),
	description varchar(255),
	title       varchar(255)
	);

create table if not exists image
(
	id         bigserial not null
	constraint image_pkey
	primary key,
	created_at timestamp not null,
	updated_at timestamp not null,
	alt_text   varchar(255),
	caption    varchar(255),
	credit     varchar(255),
	title      varchar(255),
	url        varchar(255)
	constraint uk_kjf387atbi7cqpbeq97ywna3c
	unique,
	park_id    bigint
	);

create table if not exists operating_hour
(
	id                          bigserial    not null
	constraint operating_hour_pkey
	primary key,
	created_at                  timestamp    not null,
	updated_at                  timestamp    not null,
	description                 varchar(255),
	name                        varchar(255),
	standard_hours              varchar(255) not null,
	operating_hour_exception_id bigint
	);

create table if not exists operating_hour_exception
(
	id              bigserial    not null
	constraint operating_hour_exception_pkey
	primary key,
	created_at      timestamp    not null,
	updated_at      timestamp    not null,
	end_date        timestamp,
	exception_hours varchar(255) not null,
	name            varchar(255),
	start_date      timestamp
	);

create table if not exists park
(
	id                 bigserial not null
	constraint park_pkey
	primary key,
	created_at         timestamp not null,
	updated_at         timestamp not null,
	description        varchar(255),
	designation        varchar(255),
	directions_info    varchar(255),
	directions_url     varchar(255),
	full_name          varchar(255),
	lat_long           varchar(255),
	latitude           varchar(255),
	longitude          varchar(255),
	name               varchar(255),
	park_code          varchar(255)
	constraint uk_bqeo8kcy46hsc3sd1xmsxfg3p
	unique,
	shared_id          uuid      not null
	constraint uk_109uo8u666lf6d9ed0iwt7tvw
	unique,
	states             varchar(255),
	url                varchar(255)
	constraint uk_38dc2nuwmtemhxcxlo8f0vt3q
	unique,
	weather_info       varchar(255),
	address_id         bigint
	constraint fk6ohvfqhn4spn712wfnnmxfkob
	references address,
	entance_fee_id     bigint
	constraint fkk62xbct9ve2r4upuq0uv4c7lm
	references entrance_fee,
	entrance_pass_id   bigint
	constraint fk3eegdp3uy7i3kmwg3mlp0xfq3
	references entrance_pass,
	operating_hours_id bigint
	constraint fkm5rsermqma71r43gc3yhi21sr
	references operating_hour
	);

create table if not exists phone_number
(
	id           bigserial    not null
	constraint phone_number_pkey
	primary key,
	created_at   timestamp    not null,
	updated_at   timestamp    not null,
	description  varchar(255),
	extension    varchar(255),
	phone_number varchar(255)
	constraint uk_hv4c82037jxktw54hnia2w1rm
	unique,
	type         varchar(255) not null
	);

create table if not exists topic
(
	id         bigserial not null
	constraint topic_pkey
	primary key,
	created_at timestamp not null,
	updated_at timestamp not null,
	name       varchar(255),
	shared_id  uuid      not null
	constraint uk_15a0xw1rut7ognoqr0crnd2f2
	unique,
	park_id    bigint
	);

create unique index if not exists email_index
	on email_address (email_address);
create unique index if not exists park_code_index
	on park (park_code);

alter table topic add foreign key (park_id) references park;
alter table activity add foreign key (park_id) references park;
alter table operating_hour add foreign key (operating_hour_exception_id) references operating_hour_exception;
alter table contact add foreign key (park_id) references park;
alter table contact add foreign key (phone_number_id) references phone_number;
alter table image add foreign key (park_id) references park;
alter table operating_hour add foreign key (operating_hour_exception_id) references operating_hour_exception;

