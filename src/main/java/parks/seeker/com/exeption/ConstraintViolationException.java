package parks.seeker.com.exeption;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

/**
 * custom exception for field validation
 */
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ConstraintViolationException extends RuntimeException{

	String message;

	public ConstraintViolationException(String message) {
		this.message = message;
	}
}
