package parks.seeker.com.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import parks.seeker.com.model.constant.GeneralConstants;
import parks.seeker.com.model.dto.pagination.PaginationRequest;
import parks.seeker.com.model.dto.park.CreateParkRequest;
import parks.seeker.com.model.dto.park.ParkDto;
import parks.seeker.com.model.dto.park.UpdateParkRequest;
import parks.seeker.com.service.web.ParkService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Api(tags = "park-api")
@SwaggerDefinition(tags = {
		@Tag(name = "park-api", description = "Park REST APIs")
})
@RequestMapping(value = GeneralConstants.API + "/parks")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ParkController {

	ParkService parkService;

	@ApiResponses({
			@ApiResponse(code = 200, message = "Parks successfully fetched", responseContainer = "List", response = ParkDto.class),
			@ApiResponse(code = 400, message = "Something went wrong while fetching parks")
	})
	@GetMapping
	@ApiOperation("Fetch all parks")
	public ResponseEntity<List<ParkDto>> getAllParks(@ModelAttribute PaginationRequest paginationRequest) {
		return ResponseEntity.ok(parkService.getAllParks(paginationRequest));
	}

	@ApiResponses({
			@ApiResponse(code = 200, message = "Park successfully fetched", response = ParkDto.class),
			@ApiResponse(code = 400, message = "Something went wrong while fetching park")
	})
	@GetMapping("/{parkCode}")
	@ApiOperation("Fetch park by his code")
	public ResponseEntity<ParkDto> getParkByParkCode(@PathVariable String parkCode) {
		return ResponseEntity.ok(parkService.getByParkCode(parkCode));
	}

	@ApiResponses({
			@ApiResponse(code = 201, message = "Park successfully created"),
			@ApiResponse(code = 400, message = "Failed to create park")
	})
	@ApiOperation("Create park")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void createPark(@Valid @RequestBody CreateParkRequest request) {
		parkService.createPark(request);
	}

	@ApiResponses({
			@ApiResponse(code = 200, message = "Park successfully updated"),
			@ApiResponse(code = 400, message = "Failed to update park")
	})
	@ApiOperation("Update park")
	@PutMapping("/{parkCode}")
	@ResponseStatus(HttpStatus.OK)
	public void updatePark(@Valid @RequestBody UpdateParkRequest request, @PathVariable String parkCode) {
		parkService.updatePark(request, parkCode);
	}

	@ApiResponses({
			@ApiResponse(code = 204, message = "Park successfully deleted"),
			@ApiResponse(code = 400, message = "Failed to delete park")
	})
	@ApiOperation("Delete park")
	@DeleteMapping("/{sharedId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePark(@PathVariable UUID sharedId) {
		parkService.deletePark(sharedId);
	}
}
