package parks.seeker.com.model.constant;

import lombok.experimental.UtilityClass;

/**
 * here we store constants from whole app
 */
@UtilityClass
public class GeneralConstants {

	public static final String API = "/api";
	public static final String DEV_PROFILE = "dev";
	public static final String TEST_PROFILE = "test";
	public static final String DESC_DIRECTION = "desc";
	public static final String ASC_DIRECTION = "asc";
	public static final String FULL_NAME = "fullName";
	public static final String COMMA_DELIMITER = ",";
	public static final String API_KEY_PARAM = "api_key=";
	public static final String PARK_CODE_PARAM = "parkCode=";
}
