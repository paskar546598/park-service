package parks.seeker.com.model.dto.park;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

/**
 * mock dto for updating {@link parks.seeker.com.domain.Park}
 */

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class UpdateParkRequest {

	/**
	 * Here i provide just a simple example of request dto
	 * there`s no enough time to create real object
	 * so it`ll be just a mock :)
	 */

	@NotNull
	String states;

	String weatherInfo;

	// etc
}
