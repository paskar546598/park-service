package parks.seeker.com.model.dto.pagination;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import parks.seeker.com.dictionary.PageSize;
import parks.seeker.com.model.constant.GeneralConstants;
import parks.seeker.com.validation.parkCode.ParkCodeValidation;
import parks.seeker.com.validation.stateCode.StateCodeValidation;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Base filter to enable pagination and sorting in your custom filters.
 *
 */
@Getter
@Setter
@ParkCodeValidation
@StateCodeValidation
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaginationRequest {

	/**
	 * List of park codes (each 4-10 characters in length).
	 */
	String [] parkCode;

	/**
	 *  List of 2 character state codes.
	 */
	String [] stateCode;

	/**
	 * Get the next [limit] results starting with this number. Default is 0
	 */
	@Min(value = 0L)
	@Max(value = Integer.MAX_VALUE)
	Integer start = NumberUtils.INTEGER_ZERO;

	/**
	 * Ascending direction is used by default
	 * Can be changed to descending order
	 */
	String sortDirection = GeneralConstants.ASC_DIRECTION;

	/**
	 * List of resource properties to sort the results by
	 * Default value is fullName
	 */
	String[] sort = {GeneralConstants.FULL_NAME};

	/**
	 * Term to search on
	 */
	String q;

	/**
	 * Number of results to return per request. Default is 50.
	 */
	PageSize limit = PageSize.FIFTY;

	/**
	 * Constructs sort based on the filter state
	 * @return value for {@link org.springframework.data.domain.Sort}
	 */
	public Sort getSort() {

		if (sort.length == NumberUtils.INTEGER_ZERO || StringUtils.EMPTY.equals(sort[NumberUtils.INTEGER_ZERO].trim())) {
			sort = new String[] {GeneralConstants.FULL_NAME};
		}

		return GeneralConstants.DESC_DIRECTION.equals(sortDirection) ? Sort.by(Sort.Direction.DESC, sort) : Sort.by(Sort.Direction.ASC, sort);
	}

	/**
	 * Constructs pageable based on the filter state
	 * @return value for {@link org.springframework.data.domain.PageRequest}
	 */
	public PageRequest getPageRequest() {
		return PageRequest.of(this.getStart(), this.getLimit().getValue(), getSort());
	}
}