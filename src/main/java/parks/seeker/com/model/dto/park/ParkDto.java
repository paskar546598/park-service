package parks.seeker.com.model.dto.park;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * mock dto for {@link parks.seeker.com.domain.Park}
 */

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ParkDto {

	/**
	 * Here i provide just a simple example of dto
	 * there`s no enough time to create real object
	 * so it`ll be just a mock :)
	 */

	String total;

	String limit;

	String start;
}
