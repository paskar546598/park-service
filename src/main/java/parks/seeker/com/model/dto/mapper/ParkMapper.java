package parks.seeker.com.model.dto.mapper;

import org.springframework.stereotype.Component;
import parks.seeker.com.domain.Park;
import parks.seeker.com.model.dto.park.CreateParkRequest;
import parks.seeker.com.model.dto.park.ParkDto;
import parks.seeker.com.model.dto.park.UpdateParkRequest;

import static java.util.Objects.isNull;

/**
 * custom mapper for {@link parks.seeker.com.domain.Park}
 */

@Component
public class ParkMapper {

	/**
	 * Here i provide just a simple example of custom mapper
	 * there`s no enough time to create real mapper
	 * so it`ll be just a mock :)
	 */

	public Park mapFromDtoToEntity(CreateParkRequest request) {
		if (isNull(request)) {
			return null;
		}

		Park park = new Park();
		park.setFullName("Simple example");
		return park;
	}

	public ParkDto mapFromEntityToDto(Park park) {

		if (isNull(park)) {
			return null;
		}

		ParkDto parkDto = new ParkDto();
		parkDto.setLimit(parkDto.getLimit());

		return parkDto;
	}

	public void updateEntity(UpdateParkRequest request, Park park) {
		park.setFullName("Simple example");
	}
}
