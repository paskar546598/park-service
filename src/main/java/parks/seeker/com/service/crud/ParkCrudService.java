package parks.seeker.com.service.crud;

import com.querydsl.core.BooleanBuilder;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import parks.seeker.com.domain.Park;
import parks.seeker.com.domain.QPark;
import parks.seeker.com.model.constant.GeneralConstants;
import parks.seeker.com.model.dto.pagination.PaginationRequest;
import parks.seeker.com.repository.ParkRepository;
import parks.seeker.com.service.base.DefaultCrudSupport;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

/**
 * Service layer for {@link Park} which works only with DB
 * this service has been created in order to separate service with business logic and CRUD service
 */

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ParkCrudService extends DefaultCrudSupport<Park> {

	ParkRepository parkRepository;

	public ParkCrudService(ParkRepository parkRepository) {
		super(parkRepository);
		this.parkRepository = parkRepository;
	}

	/**
	 * @param sharedId park`s UUID
	 * @return {@link Park} or throws an exception
	 */
	@Transactional(readOnly = true)
	public Park findBySharedId(@NonNull UUID sharedId) {
		return parkRepository
				.findBySharedId(sharedId)
				.orElseThrow(() -> new EmptyResultDataAccessException("Entity was not found by ID: " + sharedId, NumberUtils.INTEGER_ONE));
	}

	/**
	 * @param parkCode park`s code
	 * @return {@link Park} or throws an exception
	 */
	@Transactional(readOnly = true)
	public Optional<Park> findParkCode(@NonNull String parkCode) {
		return parkRepository.findByParkCode(parkCode);
	}

	/**
	 * finding all {@link Park} from db
	 *
	 * @param paginationRequest custom filter
	 * @return sorted and filtered data
	 */
	@Transactional(readOnly = true)
	public Page<Park> findAll(@NonNull PaginationRequest paginationRequest) {
		BooleanBuilder condition = createBooleanCondition(Arrays.asList(paginationRequest.getParkCode()), Arrays.asList(paginationRequest.getStateCode()));
		return nonNull(condition.getValue()) ?
				parkRepository.findAll(condition, paginationRequest.getPageRequest()) :
				parkRepository.findAll(paginationRequest.getPageRequest());
	}

	/**
	 * creates condition by which {@link Park} is filtered
	 *
	 * @param parkCode  this filter param comes from {@link PaginationRequest}
	 * @param stateCode this filter param comes from {@link PaginationRequest}
	 * @return {@link BooleanBuilder}
	 */
	private BooleanBuilder createBooleanCondition(List<String> parkCode, List<String> stateCode) {
		BooleanBuilder condition = new BooleanBuilder();
		QPark qPark = QPark.park;
		if (nonNull(parkCode) && parkCode.size() > INTEGER_ZERO && !parkCode.get(INTEGER_ZERO).equals(EMPTY)) {
			condition.and(qPark.parkCode.in(parkCode));
		}
		if (nonNull(stateCode) && stateCode.size() > INTEGER_ZERO && !stateCode.get(INTEGER_ZERO).equals(EMPTY)) {
			String states = String.join(GeneralConstants.COMMA_DELIMITER, stateCode);
			condition.and(qPark.states.containsIgnoreCase(states));
		}
		return condition;
	}
}
