package parks.seeker.com.service.web;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import parks.seeker.com.domain.Park;
import parks.seeker.com.model.dto.mapper.ParkMapper;
import parks.seeker.com.model.dto.pagination.PaginationRequest;
import parks.seeker.com.model.dto.park.CreateParkRequest;
import parks.seeker.com.model.dto.park.ParkDto;
import parks.seeker.com.model.dto.park.UpdateParkRequest;
import parks.seeker.com.service.crud.ParkCrudService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static parks.seeker.com.model.constant.GeneralConstants.API_KEY_PARAM;
import static parks.seeker.com.model.constant.GeneralConstants.PARK_CODE_PARAM;

/**
 * Service layer for {@link Park}
 */

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ParkService {

	ParkCrudService parkCrudService;
	ParkMapper parkMapper;
	WebClient webClient;
	String nationalParkApiKey;

	public ParkService(@Value("#{'${national.park.service.api-key}'}") String nationalParkApiKey,
			ParkCrudService parkCrudService, ParkMapper parkMapper, WebClient webClient) {
		this.nationalParkApiKey = nationalParkApiKey;
		this.parkCrudService = parkCrudService;
		this.parkMapper = parkMapper;
		this.webClient = webClient;
	}

	/**
	 * retrieves all parks from database, in case if our database is empty
	 * we make call to National Park Service API
	 *
	 * @param paginationRequest custom filter
	 * @return list of {@link ParkDto}
	 */
	@Transactional(readOnly = true)
	public List<ParkDto> getAllParks(@NonNull PaginationRequest paginationRequest) {
		List<ParkDto> parkDto = parkCrudService.findAll(paginationRequest)
				.stream()
				.map(parkMapper::mapFromEntityToDto)
				.collect(Collectors.toList());
		if (parkDto.size() == INTEGER_ZERO) {
			//parkDto would have same structure as objects we receive from get request
			//and we don`t need to map it (p.s. right now it`s just a mock)
			parkDto = getParksOnTheFly();
		}
		return parkDto;
	}

	/**
	 * entity creation from request
	 *
	 * @param request {@link CreateParkRequest}
	 */
	@Transactional
	public void createPark(@NonNull CreateParkRequest request) {
		Park park = parkMapper.mapFromDtoToEntity(request);
		parkCrudService.save(park);
	}

	/**
	 * update entity form request by park code
	 *
	 * @param request  {@link UpdateParkRequest}
	 * @param parkCode this param we receive from url-path
	 */
	@Transactional
	public void updatePark(@NonNull UpdateParkRequest request, String parkCode) {
		Optional<Park> park = parkCrudService.findParkCode(parkCode);
		if (park.isPresent()) {
			parkMapper.updateEntity(request, park.get());
			parkCrudService.save(park.get());
		} else {
			throw new EmptyResultDataAccessException
					("Resource haven't been found by provided park code: " + parkCode, NumberUtils.INTEGER_ONE);
		}

	}

	/**
	 * delete entity if it was found by sharedId
	 *
	 * @param sharedId {@link UUID}
	 */
	@Transactional
	public void deletePark(@NonNull UUID sharedId) {
		parkCrudService.delete(parkCrudService.findBySharedId(sharedId));
	}

	/**
	 * get {@link Park} by park code
	 *
	 * @param parkCode this param we receive from url-path
	 * @return {@link ParkDto}
	 */
	@Transactional(readOnly = true)
	public ParkDto getByParkCode(String parkCode) {
		Optional<Park> park = parkCrudService.findParkCode(parkCode);
		ParkDto parkDto;
		if (park.isPresent()) {
			parkDto = parkMapper.mapFromEntityToDto(park.get());
		} else {
			//parkDto would have same structure as objects we receive from get request
			//and we don`t need to map it (p.s. right now it`s just a mock)
			parkDto = getParkOnTheFlyByParkCode(parkCode);
		}
		return parkDto;
	}

	/**
	 * We use this method when we don`t have {@link Park} in our DB
	 * Here we use {@link WebClient} - it is an interface representing the main entry point for performing web requests
	 *
	 * @return list of {@link ParkDto}
	 */
	public List<ParkDto> getParksOnTheFly() {
		return webClient.get()
				.uri(String.format("?%s%s", API_KEY_PARAM, nationalParkApiKey))
				.retrieve()
				.bodyToFlux(ParkDto.class)
				.collect(Collectors.toList())
				.block();
	}

	/**
	 * find {@link Park} by park code
	 * We use this method when we don`t have {@link Park} in our DB
	 * Here we use {@link WebClient} - it is an interface representing the main entry point for performing web requests
	 *
	 * @return {@link ParkDto}
	 */
	public ParkDto getParkOnTheFlyByParkCode(String parkCode) {
		return webClient
				.get()
				.uri(String.format("?%s%s&%s%s", PARK_CODE_PARAM, parkCode, API_KEY_PARAM, nationalParkApiKey))
				.retrieve()
				.bodyToMono(ParkDto.class)
				.block();
	}
}
