package parks.seeker.com.service.base;


import parks.seeker.com.domain.base.AbstractVersional;

/**
 * Defines commonly used CRUD operations
 */

public interface CrudSupport <E extends AbstractVersional> {

    E getById(final Long entityId);

    /**
     * Updates an entity.
     * If this entity hasn't persisted yet {@link IllegalArgumentException}
     * will be thrown, so if you want to use one
     * method for create and update
     * operations use {@link #save(AbstractVersional)} instead
     *
     * @param entity entity to update
     * @return updated entity
     * @throws IllegalArgumentException if entity hasn't persisted yet
     */
    E update(final E entity);

    /**
     * Persists an entity.
     * If entity has already persisted {@link IllegalArgumentException} will
     * be thrown
     *
     * @param entity entity to create
     * @return created entity
     * @throws IllegalArgumentException if entity has already persisted
     */
    E create(final E entity);

    /**
     * Saves an entity.
     * This method resolves entity's state and
     * delegates to either {@link #create(AbstractVersional)} or {@link #update(AbstractVersional)}
     * method to do actual work
     *
     * @param entity entity to save
     * @return saved entity
     */
    E save(final E entity);

    /**
     * Removes an entity
     * If entity hasn't been persisted {@link IllegalArgumentException} will be thrown
     *
     * @param entity entity to remove
     * @throws IllegalArgumentException if entity hasn't persisted yet
     */
    void delete(final E entity);

    /**
     * Removes an entity by id
     * If entity hasn't been persisted {@link IllegalArgumentException} will be thrown
     *
     * @param entityId id of an entity to remove
     * @throws IllegalArgumentException if entity hasn't persisted yet
     */
    void deleteById(final Long entityId);
}
