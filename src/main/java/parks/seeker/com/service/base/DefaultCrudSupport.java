package parks.seeker.com.service.base;

import com.google.common.base.Preconditions;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.repository.CrudRepository;
import parks.seeker.com.domain.base.AbstractVersional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Default implementation of {@link CrudSupport} which simply delegates
 * CRUD operations to {@link CrudRepository}.
 */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public abstract class DefaultCrudSupport<E extends AbstractVersional> implements CrudSupport<E> {

    CrudRepository<E, Long> repository;

    @Override
    public E getById(Long entityId) {
        return repository
                .findById(entityId)
                .orElseThrow(() -> new RuntimeException("Entity was not found by ID: " + entityId));
    }

    @Override
    public E update(E entity) {
        Preconditions.checkArgument(nonNull(entity.getId()),
                "Could not update entity. Entity hasn't persisted yet");
        return repository.save(entity);
    }

    @Override
    public E create(E entity) {
        checkArgumentForCreate(entity);
        return repository.save(entity);
    }

    @Override
    public E save(E entity) {
        return isNull(entity.getId()) ? create(entity) : update(entity);
    }

    @Override
    public void delete(E entity) {
        Preconditions.checkArgument(nonNull(entity.getId()),
                "Could not delete entity. Entity hasn't been persisted yet");
        repository.delete(entity);
    }

    @Override
    public void deleteById(Long entityId) {
        Preconditions.checkArgument(nonNull(entityId),
                "Could not delete entity. Entity hasn't been persisted yet");
        repository.deleteById(entityId);
    }

    protected void checkArgumentForCreate(E entity) {
        Preconditions.checkArgument(isNull(entity.getId()),
                "Could not create entity. Entity has already exists");
    }
}
