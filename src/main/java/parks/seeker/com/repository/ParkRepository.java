package parks.seeker.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import parks.seeker.com.domain.Park;

import java.util.Optional;
import java.util.UUID;

/**
 * DAO layer for {@link Park}
 */

public interface ParkRepository extends JpaRepository<Park, Long>, QuerydslPredicateExecutor<Park> {

	Optional<Park> findBySharedId(UUID sharedId);

	Optional<Park> findByParkCode(String parkCode);
}
