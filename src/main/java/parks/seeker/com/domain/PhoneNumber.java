package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.dictionary.PhoneNumbersType;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * represents information about phone numbers for {@link Contact}
 */
@Entity
@Table(name = "phone_number")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhoneNumber extends AbstractVersional {

	@Column(unique = true)
	String phoneNumber;

	String description;

	String extension;

	@Column(nullable = false)
	@Enumerated(value = EnumType.STRING)
	PhoneNumbersType type;

	@OneToOne(mappedBy = "phoneNumbers")
	Contact contact;
}
