package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

/**
 *  Represents topics for the {@link Park}
 */
@Entity
@Table(name = "topic")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Topic extends AbstractVersional {

	@Column(unique = true, name = "shared_id", nullable = false)
	UUID sharedId = UUID.randomUUID(); /* The field is used for sharing between requests instead of db's id. */

	String name;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "park_id")
	Park park;
}
