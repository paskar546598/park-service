package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.dictionary.DaysOfWeek;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Represents closed dates for {@link OperatingHours}
 */
@Entity
@Table(name = "operating_hour_exception")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OperatingHoursExceptions extends AbstractVersional {

	String name;

	LocalDateTime startDate;

	LocalDateTime endDate;

	@Column(nullable = false)
	@Enumerated(value = EnumType.STRING)
	DaysOfWeek exceptionHours;

	@OneToOne(mappedBy = "exceptionHours")
	OperatingHours operatingHours;
}
