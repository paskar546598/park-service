package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Information about contacting the {@link Park}
 */
@Entity
@Table(name = "contact")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Contact extends AbstractVersional {

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "phone_number_id")
	PhoneNumber phoneNumbers;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "email_address_id")
	EmailAddress emailAddresses;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "park_id")
	Park park;
}
