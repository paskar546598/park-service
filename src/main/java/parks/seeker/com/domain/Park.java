package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

/**
 * This entity represents park
 */

@Entity
@Table(name = "park", indexes = @Index(name = "park_code_index", columnList = "parkCode"))
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Park extends AbstractVersional {

	/**
	 * State(s) the park is located in (comma-delimited list)
	 */
	String states;

	/**
	 * General description of the weather in the park over the course of a year
	 */
	String weatherInfo;

	/**
	 * General overview of how to get to the park
	 */
	String directionsInfo;

	/**
	 * Park`s addresses (physical and mailing)
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	Address addresses;

	/**
	 * Fee for entering the park
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "entance_fee_id")
	EntranceFee entranceFees;

	/**
	 * Park`s topics
	 */
	@OneToMany(mappedBy = "park", fetch = FetchType.LAZY)
	List<Topic> topics;

	/**
	 * Short park name (no designation)
	 */
	String name;

	/**
	 * Park`s latitude
	 * example: 39.9818229675293
	 */
	String latitude;

	/**
	 * Park`s longitude
	 * example: -84.0711364746094
	 */
	String longitude;

	/**
	 * Park`s activities
	 */
	@OneToMany(mappedBy = "park", fetch = FetchType.LAZY)
	List<Activity> activities;

	/**
	 * Hours and seasons when the park is open or closed
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "operating_hours_id")
	OperatingHours operatingHours;

	/**
	 * Park's Website
	 */
	@Column(unique = true)
	String url;

	/**
	 * Information about contacting the park
	 */
	@OneToMany(mappedBy = "park", fetch = FetchType.LAZY)
	List<Contact> contacts;

	/**
	 * Passes available to provide entry into the park
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "entrance_pass_id")
	EntrancePass entrancePasses;

	/**
	 * A variable width character code used to identify a specific park
	 */
	@Column(unique = true)
	String parkCode;

	/**
	 * Type of designation (eg, national park, national monument, national recreation area, etc)
	 */
	String designation;

	/**
	 * Park`s images
	 */
	@OneToMany(mappedBy = "park", fetch = FetchType.LAZY)
	List<Image> images;

	/**
	 * Full park name (with designation)
	 */
	String fullName;

	/**
	 * Park GPS coordinates
	 * example: lat:39.9818229675293, long:-84.0711364746094
	 */
	String latLong;

	/**
	 * Park identification string
	 */
	@Column(unique = true, name = "shared_id", nullable = false)
	UUID sharedId = UUID.randomUUID(); /* The field is used for sharing between requests instead of db's id. */

	/**
	 * Link to page, if available, that provides additional detail on getting to the park
	 */
	String directionsUrl;

	/**
	 * Introductory paragraph from the park homepage
	 */
	String description;
}
