package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * represents information about email for {@link Contact}
 */
@Entity
@Table(name = "email_address")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmailAddress extends AbstractVersional {

	@Column(unique = true)
	String emailAddress;

	String description;

	@OneToOne(mappedBy = "emailAddresses")
	Contact contact;
}
