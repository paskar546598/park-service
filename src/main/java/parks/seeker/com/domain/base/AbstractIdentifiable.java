package parks.seeker.com.domain.base;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Base class for all application entities
 * which have unique identifier
 */
@MappedSuperclass
@Getter
@Setter
@ToString(of = "id")
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class AbstractIdentifiable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true)
    Long id;

}