package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Passes available to provide entry into the {@link Park}
 */
@Entity
@Table(name = "entrance_pass")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EntrancePass extends AbstractVersional {

	String cost;

	String description;

	String title;
}
