package parks.seeker.com.domain;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.dictionary.AddressType;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * represents {@link Park} addresses (physical and mailing)
 */
@Entity
@Table(name = "address")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Address extends AbstractVersional {

	String postalCode;

	String city;

	String stateCode;

	String line1;

	@Column(nullable = false)
	@Enumerated(value = EnumType.STRING)
	AddressType type;

	String line2;

	String line3;

	@OneToOne(mappedBy = "addresses")
	Park park;
}
