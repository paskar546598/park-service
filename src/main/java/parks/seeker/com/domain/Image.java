package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * {@link Park} images
 */
@Entity
@Table(name = "image")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Image extends AbstractVersional {

	String credit;

	String altText;

	String title;

	String caption;

	@Column(unique = true)
	String url;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "park_id")
	Park park;

}
