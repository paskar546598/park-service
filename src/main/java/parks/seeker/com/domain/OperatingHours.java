package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.dictionary.DaysOfWeek;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Hours and seasons when the {@link Park} is open or closed
 */
@Entity
@Table(name = "operating_hour")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OperatingHours extends AbstractVersional {

	String name;

	String description;

	@Column(nullable = false)
	@Enumerated(value = EnumType.STRING)
	DaysOfWeek standardHours;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "operating_hour_exception_id")
	OperatingHoursExceptions exceptionHours;

	@OneToOne(mappedBy = "operatingHours", cascade = CascadeType.ALL)
	Park park;

}
