package parks.seeker.com.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import parks.seeker.com.domain.base.AbstractVersional;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *  Fee for entering the {@link Park}
 */
@Entity
@Table(name = "entrance_fee")
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EntranceFee extends AbstractVersional {

	String cost;

	String description;

	String title;

	@OneToOne(mappedBy = "entranceFees")
	Park park;
}
