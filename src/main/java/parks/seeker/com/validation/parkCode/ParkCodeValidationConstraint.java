package parks.seeker.com.validation.parkCode;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.math.NumberUtils;
import parks.seeker.com.exeption.ConstraintViolationException;
import parks.seeker.com.model.dto.pagination.PaginationRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.IntStream;

/**
 * custom validator for {@link parks.seeker.com.model.dto.pagination.PaginationRequest}
 * it checks length for each park code
 */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ParkCodeValidationConstraint implements ConstraintValidator<ParkCodeValidation, PaginationRequest> {


	@Override
	public void initialize(ParkCodeValidation constraintAnnotation) {
		/*Do nothing*/
	}

	@Override
	public boolean isValid(PaginationRequest request, ConstraintValidatorContext context) {
		String[] parkCode = request.getParkCode();

		IntStream.range(NumberUtils.INTEGER_ZERO, parkCode.length)
				.filter(i -> parkCode[i].length() < 4 || parkCode[i].length() > 11)
				.forEachOrdered(i -> {
			throw new ConstraintViolationException("Park code should between 4 and 10 characters ");
		});
		return true;
	}

}
