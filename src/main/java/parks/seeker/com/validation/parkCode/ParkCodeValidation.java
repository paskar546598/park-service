package parks.seeker.com.validation.parkCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * custom annotation for validation {@link parks.seeker.com.model.dto.pagination.PaginationRequest}
 * in order to understand how it works - check this class {@link ParkCodeValidationConstraint}
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ParkCodeValidationConstraint.class)
@Documented
public @interface ParkCodeValidation {

	String message() default "Length of park code should be 4 - 10 characters ";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
