package parks.seeker.com.validation.stateCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * custom annotation for validation {@link parks.seeker.com.model.dto.pagination.PaginationRequest}
 * in order to understand how it works - check this class {@link StateCodeValidationConstraint}
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StateCodeValidationConstraint.class)
@Documented
public @interface StateCodeValidation {

	String message() default "Length of state code should be 2 characters ";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
