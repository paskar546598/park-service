package parks.seeker.com.validation.stateCode;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.math.NumberUtils;
import parks.seeker.com.exeption.ConstraintViolationException;
import parks.seeker.com.model.dto.pagination.PaginationRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.IntStream;

/**
 * custom validator for {@link parks.seeker.com.model.dto.pagination.PaginationRequest}
 * it checks length for each state code
 */
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StateCodeValidationConstraint implements ConstraintValidator<StateCodeValidation, PaginationRequest> {


	@Override
	public void initialize(StateCodeValidation constraintAnnotation) {
		/*Do nothing*/
	}

	@Override
	public boolean isValid(PaginationRequest request, ConstraintValidatorContext context) {
		String[] stateCode = request.getStateCode();

		IntStream.range(NumberUtils.INTEGER_ZERO, stateCode.length)
				.filter(i -> stateCode[i].length() != NumberUtils.INTEGER_TWO)
				.forEachOrdered(i -> {
					throw new ConstraintViolationException("State code should contains 2 characters");
				});
		return true;
	}

}
