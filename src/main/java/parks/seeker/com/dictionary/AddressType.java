package parks.seeker.com.dictionary;

import lombok.AllArgsConstructor;
import lombok.Getter;
import parks.seeker.com.domain.Address;

/**
 * Represents the address type of {@link Address}.
 */
@Getter
@AllArgsConstructor
public enum AddressType {

	PHYSICAL("Physical"),
	MAILING("Mailing");

	String name;

	/**
	 * We get the address type name in string form,
	 * in order to save it properly into DB we transform it into enum value
	 * if there is no match - {@link java.lang.IllegalArgumentException} will be thrown
	 *
	 * @param name - expected address type
	 * @return enum value which we got from param
	 */
	public static AddressType getEnumValue(String name) {
		AddressType addressType;
		switch (name) {
			case "Physical":
				addressType = PHYSICAL;
				break;
			case "Mailing":
				addressType = MAILING;
				break;
			default:
				throw new IllegalArgumentException("Unexpected address type");
		}
		return addressType;
	}
}
