package parks.seeker.com.dictionary;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * simply represents types of phone numbers
 */
@Getter
@AllArgsConstructor
public enum PhoneNumbersType {

	VOICE("Voice"),
	FAX("Fax"),
	TTY("TTY");

	String phoneNumberType;

	/**
	 * We get the address type name in string form,
	 * in order to save it properly into DB we transform it into enum value
	 * if there is no match - {@link java.lang.IllegalArgumentException} will be thrown
	 *
	 * @param phoneNumberType - expected address type
	 * @return enum value which we got from param
	 */
	public static PhoneNumbersType getEnumValue(String phoneNumberType) {
		PhoneNumbersType numbersType;
		switch (phoneNumberType) {
			case "Voice":
				numbersType = VOICE;
				break;
			case "Fax":
				numbersType = FAX;
				break;
			case "TTY":
				numbersType = TTY;
				break;
			default:
				throw new IllegalArgumentException("Unexpected phone number type");
		}
		return numbersType;
	}

}
