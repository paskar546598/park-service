package parks.seeker.com.dictionary;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents the page size for {@link parks.seeker.com.model.dto.pagination.PaginationRequest}.
 */
@Getter
@AllArgsConstructor
public enum PageSize {

	FIVE(5),
	TEN(10),
	TWENTY(20),
	FIFTY(50),
	ONE_HUNDRED(100);

	Integer value;
}
