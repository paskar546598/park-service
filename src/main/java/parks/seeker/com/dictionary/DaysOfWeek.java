package parks.seeker.com.dictionary;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * simply represents days of the week
 */
@Getter
@AllArgsConstructor
public enum DaysOfWeek {

	SUNDAY("sunday"),
	MONDAY("monday"),
	TUESDAY("tuesday"),
	WEDNESDAY("wednesday"),
	THURSDAY("thursday"),
	FRIDAY("friday"),
	SATURDAY("saturday");

	String dayOfWeek;

	/**
	 * We get the day of the week in string form,
	 * in order to save it properly into DB we transform it into enum value
	 * if there is no match - {@link java.lang.IllegalArgumentException} will be thrown
	 *
	 * @param dayOfWeek - expected address type
	 * @return enum value which we got from param
	 */
	public static DaysOfWeek getEnumValue(String dayOfWeek) {
		DaysOfWeek daysOfWeek;
		switch (dayOfWeek) {
			case "sunday":
				daysOfWeek = SUNDAY;
				break;
			case "monday":
				daysOfWeek = MONDAY;
				break;
			case "tuesday":
				daysOfWeek = TUESDAY;
				break;
			case "wednesday":
				daysOfWeek = WEDNESDAY;
				break;
			case "thursday":
				daysOfWeek = THURSDAY;
				break;
			case "friday":
				daysOfWeek = FRIDAY;
				break;
			case "saturday":
				daysOfWeek = SATURDAY;
				break;
			default:
				throw new IllegalArgumentException("Unexpected day of the week value");
		}
		return daysOfWeek;
	}
}
