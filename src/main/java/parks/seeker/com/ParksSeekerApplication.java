package parks.seeker.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "parks.seeker.com", proxyBeanMethods = false)
public class ParksSeekerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParksSeekerApplication.class, args);
	}

}
