## Setup instructions:

General Tips on How to Run an App on Java:
1) Install and Setup JDK-8 (java 8 on this project)
2) Install Maven
Locate file pom.xml (it is at the root of the folder) - If the icon is not in the form of letter M - do next steps:
   a) Right-click the pom.xml file
   b) Select - "Add as Maven project"
3) src/main - folder "java" must be blue (if no -> Right-click -> select "Mark directory as Source root")
4) src/main - folder "resources" must have small gold sing (if no -> Right-click -> select "Mark directory as Resources root")
5) src/main/java - folder "java" must be green (if no -> Right-click -> select "Mark Test directory as Source root")

To be able to run this application, perform the following 2 required steps and 1 optional (recommended):
1. Install PostgreSQL 12. Database user: postgres; password: postgres
   Create a database with the name ‘postgres’. Do it by command line or pgAdmin tool
   Connect the database (name of DB can be found in the application-dev.yml)
   a) Locate the Databases icon in your compiler and click on it
   b) Press "+" sign, then Data Source - select PostgreSQL 
   c) UserName, password and database name you can find in application-dev.yml file (postgres default)
2. You must specify one more settings:
   a) Open Spring Boot configurations (press "edit configurations" button)
   b) "-Dspring.profiles.active=dev" - you need to add this line into "VM options" field in order to be run this app
   
I would recommend you to set one more useful thing:
   a) Open Spring Boot configurations (press "edit configurations" button)
   b) Look for "Before launch" section, press "+" sign and select "Run Maven Goal"
   c) Into "Command line" input write next command - "clean install -DskipTests"
   Now the Maven commands will run before each run of this app

## How did you decide which technologies to use as part of your solution?
1) Spring Boot - I`ve decided use spring boot because It provides Java developers with a platform to get started with an auto configurable production-grade Spring application.
   With it, I started quickly without losing time on preparing and configuring their Spring application.
2) Hibernate - easy to use, one of the best (if not the best) ORM framework for mapping an object-oriented domain model to a relational database,
   this tool is free, it has very great possibilities in use
3) querydsl - one more ORM framework - I`ve decided use it because its easy create filtering conditions with it, also has great possibilities in use
4) liquibase - the most popular framework for tracking, managing and applying database schema changes.
   No one would use  hibernate.ddl-auto: update flag, in this app I used it just for example, no time for creating sql scripts for liquibase
5) Lombock - great tool for avoiding lots of redundant java code (e.g. seters, geters, toString, constructors etc)
6) postgresql - its free and has great possibilities in use (mysql also would be fine)
7) swagger - in order to create API documentation (never worked with alternative libraries that is why I chose this)

## Are there any improvements you could make to your submission?
1) A lot of mocks in this app, not enough time to do this task completely - would be great to create services for all entities
2) Config logger for prod server
3) Add validation for dto objects (right now its mocks objects without)
4) I would use MapStruct for mapping
5) Somewhere in this app - fetch = FetchType.EAGER, I would change it to fetch = FetchType.LAZY and as a result I would have 
   Hibernate N + 1 problem, but performance for whole an app would better, except some queries with hibernate N + 1 issue
   This issue I would solve by:
   a) fetch join
   b) entity graphs
   c) query batching
6) I would write tests more tests, especially for controller
 
## What would you do differently if you were allocated more time?
Everything I would like to improve in this task - I wrote above
One thing I would do definitely differently - change mocks objects into real
I would not make such huge commits, as I did